// Search function
function getValue() {
    const searchDocumentValue = document.querySelector(".search-bar").value;
    // stop page from reloading
    document.querySelector("form").addEventListener("submit", function (event) {
        event.preventDefault();
    }, false);
    window.find(searchDocumentValue);
}


// Carousel
const randomNumber = Math.floor(Math.random() * 5);
const carouselDocument = document.querySelector(`.item${randomNumber}`);
carouselDocument.classList.add("active");

// Read More
function readMore(n) {
    const showMoreDocument = document.querySelector(`.read-more${n}`);
    if (showMoreDocument.style.display === "inline") {
        return showMoreDocument.style.display = "none";
    } else {
        return showMoreDocument.style.display = "inline";
    }
};

// Scoreboard
const winners = ["2021 - ?", "2020 - Denmark", "2019 - Italy", "2018 - Romania", "2017 - Norway", "2016 - Denmark", "2015 - Denmark"];

winners.forEach(item => {
    item = `<li>${item}</li>`;
    document.querySelector(".winners").innerHTML += item;
})

// Sponsors

const sponsorsDocument = document.querySelector(".sponsors");
for (let i = 1; i < 23; i++) {
    const sponsorsHolder = document.createElement("div");
    const sponsorsImages = document.createElement("img");
    const setImage = sponsorsImages.setAttribute("src", `./images/sponsors/sponsors${i}.png`);
    sponsorsHolder.classList.add("sponsors-place", "my-3", "mx-2");
    sponsorsHolder.append(sponsorsImages);
    sponsorsDocument.append(sponsorsHolder);
}
